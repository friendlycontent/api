$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require_relative "lib/friendlycontent/api/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "friendlycontent-api"
  s.version     = Friendlycontent::Api::VERSION
  s.authors     = ["Max Ivak"]
  s.email       = ["maxivak@gmail.com"]
  s.homepage    = "https://gitlab.com/friendlycontent/api"
  s.summary     = "FriendlyContent.io API."
  s.description = "Wrapper for API for FriendlyContent.io. Get content from content management system FriendlyContent.io"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  # s.add_dependency 'zeitwerk'

  s.add_dependency "rails", ">= 5.1"

  s.add_dependency "httparty"
  s.add_dependency "rest-client"
end
