module Friendlycontent
  module Model
    module Settings
      class DataFields
        attr_accessor :fields

        def initialize
          @fields = {}
        end

        def field(name, opts, &block)
          @fields[name] = opts
        end

      end
    end
  end
end
