module Friendlycontent
  module Model
    module Settings
      class Data
        attr_accessor :settings
        attr_accessor :fields

        def initialize(settings={})
          @settings = settings
        end

        def to_hash
          @settings
        end

        def as_json(options={})
          to_hash
        end

        def method_missing(method_name, *arguments, &block)
          if settings.key?(method_name.to_sym)
            return settings[method_name.to_sym]
          end
          super
        end

        def fields(&block)
          return @fields unless @fields.nil?

          @fields = Friendlycontent::Model::Settings::DataFields.new

          if block_given?
            @fields.instance_eval(&block)
          end
        end

        def get_option(name)
          @settings[name.to_sym]
        end

        def set_option(name, v)
          @settings[name.to_sym] = v
        end

        def key_field
          settings[:key_field] || :id
        end
      end
    end
  end
end

