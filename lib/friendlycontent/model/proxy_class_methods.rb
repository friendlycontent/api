module Friendlycontent
  module Model
  # A proxy interfacing between Friendlycontent::Model class methods and model class methods
    class ProxyClassMethods
      #include Friendlycontent::Model::ProxyBase

      attr_reader :target

      def initialize(target)
        @target = target
      end

      # Delegate methods to `@target`
      # def method_missing(method_name, *arguments, &block)
      #   target.respond_to?(method_name) ? target.__send__(method_name, *arguments, &block) : super
      # end
      #
      # # Respond to methods from `@target`
      # def respond_to?(method_name, include_private = false)
      #   target.respond_to?(method_name) || super
      # end

      def inspect
        "[PROXY] #{target.inspect}"
      end

      include Friendlycontent::Model::Concerns::Client::ClassMethods
      include Friendlycontent::Model::Concerns::Settings::ClassMethods
      include Friendlycontent::Model::Concerns::DataOperations::ClassMethods
      include Friendlycontent::Model::Concerns::ProxyVproperties::ClassMethods

    end
  end
end
