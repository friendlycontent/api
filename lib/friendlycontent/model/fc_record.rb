module Friendlycontent
  module Model
    class FcRecord
      attr_accessor :id
      attr_accessor :versions
      attr_accessor :fields

      # for deserializing from FC Repository
      def from_hash(data)
        @id = data['id']
        @fields = data['fields']
        @versions = data['versions'] || {}

        self
      end

      def method_missing(method_name, *arguments, &block)
        if fields.has_key?(method_name.to_s)
          return field_value(method_name.to_s, *arguments)
        else
          super
        end
      end

      def field_value(field_name, version=nil)
        vv = fields[field_name] || nil

        return nil if vv.nil?

        if !vv.is_a?(Hash)
          return vv
        end

        if vv.is_a?(Hash)
          # can be complex non-versioned type (image) or a versioned field
          if version.nil?
            return vv
          end

          # vversion = version || 'default'
          # v = vv.dig(vversion)
          v = vv.dig(version)
          # return nil if v.nil?

          return v
        end

        raise 'unexpected'
        nil
      end
    end
  end
end
