module Friendlycontent
  module Model
    class Vproperties < ::ActiveRecord::Base
      class << self
        # Sometimes ActiveRecord calls .table_exists? before the table name
        def table_exists?
          table_name.present? && super
        end

      end

    end
  end
end

