module Friendlycontent
module Model
  module Concerns
    module Client

      module ClassMethods
        # Get the client for a specific model class
        def client
          @client ||= Friendlycontent::Model::ModelBase.client
          # @client ||= Friendlycontent::Api::Clientnew.new({})
          @client
        end

        def client=(_client)
          @client = _client
        end
      end

      module InstanceMethods
        # def client
        #   @client ||= self.class.client
        # end
        #
        def client
          @client ||= target.class.__friendlycontent__.client
          @client
        end

        def client=(_client)
          @client = _client
        end
      end
    end
  end
  end
end
