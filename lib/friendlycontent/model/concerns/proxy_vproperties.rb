module Friendlycontent
  module Model
    module Concerns
      module ProxyVproperties

        module ClassMethods
          def vproperties_class(version)
            @vproperties_classes ||= {}

            @vproperties_classes[version] ||= begin
              name = vproperties_class_name(version)

              if target.const_defined?(name.to_sym, false)
                klass = target.const_get(name.to_sym, false)
              else
                klass = target.const_set(name.to_sym, Class.new(Friendlycontent::Model::Vproperties))
              end

              klass.belongs_to :entity,
                              class_name: target.name,
                              foreign_key: :entity_id,
                              inverse_of: vproperties_name(version)
              klass
            end
          end

          def vproperties_name(version)
            "#{version}_props".to_sym
          end

          def vproperties_class_name(version)
            "#{version.to_s.classify}Props"
          end

          def vproperties_table_name(version)
            "#{target.table_name.singularize}_#{version}_props"
          end
        end

        module InstanceMethods

        end
      end
    end
  end
end

