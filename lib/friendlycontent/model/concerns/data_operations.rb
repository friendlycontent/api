module Friendlycontent
  module Model
  module Concerns
    module DataOperations

      module ClassMethods

        # TODO: move to a separate concern
        def define_field_attributes(opts={})
          all_fields = content_type_fields
          skip_fields = opts[:skip_fields] || []

          all_fields.each do |field|
            field_name = field.name
            next if skip_fields.include?(field_name)

            target.define_method(field_name) do |version_opts=nil|
              return __friendlycontent__.get_field_value(field_name, version_opts)
            end
          end
        end



        def build_request_param_locale(opt_locale)
          if opt_locale.nil?
            return nil
          end

          if opt_locale
            locale = opt_locale
            if locale.nil?
              return nil
            elsif locale.is_a?(String)
              p_locale = locale
            elsif locale.is_a?(Array)
              p_locale = locale.join(",")
            end
          end

          return p_locale
        end

        # @return [Array]
        def build_request_param_version(opt_version)
          return nil if opt_version.nil?

          raise 'expected Hash' if !opt_version.is_a?(Hash)

          res = {}
          opt_version.each do |k,v|
            values = nil
            if v.is_a?(String)
              values = [v]
            elsif v.is_a?(Array)
              values = v
            end

            res[k] = values
          end
          return res
        end

        def load(id, opts={})
          object = target.find_by(id: id) || target.new(id: id)
          object.__friendlycontent__.load(opts)
          object
        end

        def find(id, opts={})
          request_params = {}
          if opts.has_key?(:locale)
            request_params[:locale] = build_request_param_locale(opts[:locale])
          end
          if opts.has_key?(:key_field)
            request_params[:key_field] = opts[:key_field]
          end

          response = client.content_get_record settings.project_path, settings.content_type, id, request_params

          if response.status != 200
            return nil
          end

          object = @target.new
          #object.__friendlycontent__.target_init_id response.payload['id']
          #object.__friendlycontent__.load_from_fc_data response.payload
          object.__friendlycontent__.load_record_from_data response.payload

          object
        end

        def search(filter={}, opts={})
          response = search_raw filter, opts

          if response.status != 200
            return nil
          end

          load_records(response)
        end

        def search_raw(filter={}, opts={})
          response = client.content_search settings.user, settings.project, settings.content_type, filter, opts
        end

        def load_records(response)
          records = []

          list = response.payload
          list.each do |item|
            object = @target.new
            object.__friendlycontent__.target_init_id item['id']
            object.__friendlycontent__.load_from_fc_data item
            records << object
          end

          records
        end

        #
        # opts:
        # from: from timestamp
        # batch_size
        def sync(opts = {})
          # TODO:

        end

        def project_path
          settings.project_path
        end

        def content_type
          settings.content_type
        end

        def schema
          @@schema ||= load_schema
        end

        def load_schema
          schema = Friendlycontent::Model::Schema.new

          schema.fields = content_type_fields
          schema.versions = content_type_versions
          schema.relations = content_type_relations

          schema
        end


        def content_type_fields(filter={})
          response = client.content_type_fields(project_path, content_type)

          if response.status != 200
            return nil
          end

          load_fields_list response
        end

        def load_fields_list(response)
          list = response.payload
          list.map{|field_data| Friendlycontent::FcModel::ContentTypeField.create_from_data(field_data) }
        end

        def content_type_versions(filter={})
          response = client.content_type_versions(project_path, content_type)

          if response.status != 200
            return nil
          end

          load_versions_list response
        end

        def load_versions_list(response)
          list = response.payload
          list.map{|version_data| Friendlycontent::FcModel::ContentTypeVersion.create_from_data(version_data) }
        end

        def content_type_relations(filter={})
          response = client.content_type_relations(project_path, content_type)

          if response.status != 200
            return nil
          end

          load_relations_list response
        end

        def load_relations_list(response)
          list = response.payload
          list.map{|relation_data| Friendlycontent::Model::ContentTypeRelation.create_from_data(relation_data) }
        end
      end

      module InstanceMethods

        def load(opts={})
          request_params = {}
          # if opts.has_key?(:locale)
          #   request_params[:locale] = self.class.build_request_param_locale(opts[:locale])
          # end

          versions = nil
          if opts.has_key?(:versions)
            versions = opts[:versions]
            request_params[:versions] = self.class.build_request_param_version(opts[:versions])
          end

          response = client.content_get_record settings.project_path, settings.content_type, __friendlycontent__.id, request_params

          if response.status != 200
            return nil
          end

          object = @target
          object.__friendlycontent__.load_record_from_data(response.payload)
          #object.__friendlycontent__.load_from_fc_data response.payload

          set_versions_loaded versions

          object
        end

        def download(opts={})
          load(opts)
          @target.save
        end

        def loaded?(version_name=nil, version_value=nil)
          @versions_loaded ||= {}

          if version_name.nil? || version_value.nil?
            return !@versions_loaded.dig('default', 'default').nil?
          end

          version_name ||= 'default'
          version_value ||= 'default'

          return !@versions_loaded.dig(version_name, version_value).nil?
        end

        # can set multiple versions
        # ex. versions = {locale: 'fr'}
        # versions = {locale: ['en', 'fr']}
        # versions = {locale: ['en', 'fr'], image_version: ['thumb', 'medium']}
        def set_versions_loaded(versions)
          @versions_loaded ||= {}

          if versions.nil?
            set_version_loaded nil, nil
            return
          end

          if !versions.is_a?(Hash)
            raise 'expected Hash'
          end

          versions.each do |version_name, version_values|
            if version_values.is_a?(Array)
              version_values.each do |v_value|
                set_version_loaded version_name, v_value
              end
            else
              # version_values - scalar
              set_version_loaded version_name, version_values
            end
          end
        end

        def set_version_loaded(version_name=nil, version_value = nil)
          v_version_name = version_name || 'default'
          v_version_value = version_value || 'default'
          @versions_loaded[v_version_name] ||= {}
          @versions_loaded[v_version_name][v_version_value] = true
        end

        ### TODO: move to FcRecord
        def fields
          return @fields unless @fields.nil?
          @fields ||= {}
        end

        def get_field_value(name, version_opts=nil)
          if version_opts.nil?
            load unless loaded?

            return field_value name
          end

          # TODO:
          # find main version for the field. get from field metadata
          # version_name = field.versions[0]

          if !version_opts.is_a?(Hash)
            raise 'TODO: not supported. expected Hash'
          end

          if version_opts.is_a?(Hash)
            # ex. version_opts={locale: 'en'}

            if !version_opts.keys.count == 1
              raise 'unexpected version_opts format: should be one version'
            end

            version_name = version_opts.keys[0]
            version_value = version_opts[version_name]

            # check if needed to load for the version
            load({versions: version_opts}) unless loaded?(version_name, version_value)

            return field_value name, version_value
          end

          raise 'unexpected'
        end

        def field_value(name, version_value=nil)
          name = name.to_s
          field_values = fields[name]

          return nil if field_values.nil?

          return field_values if !field_values.is_a?(Hash)

          if version_value.nil?
            if field_values.has_key?('default')
              return field_values['default']
            else
              return field_values
            end
          else
            return field_values[version_value]
          end

          nil
        end

        def versions
          return @versions unless @versions.nil?
          @versions ||= {}
        end

        def init_id
          key_field = settings.key_field
          if @target.respond_to?(key_field)
            @id = @target.send(key_field)
          end

          @id
        end

        def target_init_id(_id)
          key_field = settings.key_field
          @target.send("#{key_field}=", _id)
        end

        def id
          @id ||= 0
        end

        def set_field_value(name, v, opts={})
          name = name.to_s
          field_value = fields[name]
          if field_value.nil? || !field_value.is_a?(Hash)
            fields[name] = v
          else
            # set by locale
            locale = opts[:locale] || 'default'
            fields[name][locale] = v
          end
        end

        def field_value=(name, v)
          set_field_value(name, v, {locale: nil})
        end

        def links
          return @links unless @links.nil?
          @links ||= {}
        end

        ### TODO: review. deprecate. use load_record_from_data
        # move to FcRecord
        def load_from_fc_data(data)
          @id = data['id']

          # init
          @versions ||= {}
          @fields ||= {}
          @links ||= {}

          # sys fields
          @fields['id'] = data['id']
          @fields['status'] = data['status']

          # fields
          field_values = data['fields'] || {}
          field_values.each do |f,v|
            # merge. Hash - it is values by locales
            if v.is_a?(Hash)
              @fields[f] ||= {}
              @fields[f].merge!(v)
            else
              @fields[f] = v
            end
          end

          # versions
          @versions = data['versions'] || {}

          # links
          data_links = data['links'] || {}
          data_links.each do |link_name, link_url|
            @links[link_name] = link_url
          end
        end

        # def to_record_data(fc_record)
        #   data = {}
        #
        #   fc_record.fields.each do |f, values|
        #     next if f == 'id'
        #     if values.is_a?(Hash)
        #       data[f] = Eazyplan::Data::FieldValueObject::VersionedFc.new(values)
        #     elsif values.is_a?(Array)
        #       raise 'unexpected data'
        #     else
        #       # scalar
        #       data[f] = Eazyplan::Data::FieldValueObject::Scalar.new(values)
        #     end
        #   end
        #
        #   data
        # end

        def load_record_from_data(data)
          # set attributes for AR object

          schema = self.class.schema

          # sys fields
          key_field = settings.key_field
          @target.write_attribute(key_field, data['id'])

          # data

          versions = data['versions'] || {}

          # vproperties_classes = {}
          props_relations = {}

          versions.each do |version_name, version_values|
            props_relations[version_name] = self.class.vproperties_name(version_name)
            # vproperties_classes[version_name] = self.class.vproperties_class(version_name)
          end

          # cache_vproperties = {}

          # versions.each do |version_name, version_values|
          #   record_props = vproperties_by_version[version_name][ver]
          # end


          # fields
          field_values = data['fields'] || {}

          field_values.each do |f,v|
            field = schema.fields_by_name[f]

            if v.is_a?(Hash)
              # versioned field values

              version_name = field.versions[0]

              rel_name = props_relations[version_name]
              rel = @target.send(rel_name)

              v.each do |version_value, prop_value|
                # TODO: handle when version_value is nil
                obj_props = rel.select{|x| x.version == version_value}.first || rel.build(version: version_value)
                obj_props.write_attribute(f, prop_value)
                #obj_props = cls_props.where(entity_id: @target.id, version: version_name).first || cls_props.new(entity_id: @target.id, version: version_name)
              end

              #@fields[f] ||= {}
              #@fields[f].merge!(v)
            else
              if @target.has_attribute?(f)
                @target.write_attribute(f, v)
              end
            end

          end


          # links
          # TODO: implement links to other records
          # data_links = data['links'] || {}
          # data_links.each do |link_name, link_url|
          #   @links[link_name] = link_url
          # end


          x = 0
        end

        def save(_new_fields=nil)
          new_fields = _new_fields || fields

          data = {
              fields: new_fields
          }
          _settings = settings
          response = client.content_update_record _settings.user, _settings.project, _settings.content_type, self.id, data

          if response.status != 200
            return false
          end

          true
        end
      end

    end
  end
  end
end
