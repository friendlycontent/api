module Friendlycontent
  module Model
  module Concerns
    module Settings

      module ClassMethods
        def settings
          @settings
        end

        def set_settings(options={}, &block)
          if options.respond_to?(:read)
            options = YAML.load(options.read)
          end

          # ignore if already set up
          if @settings.present?
            return
          end

          @settings ||= Friendlycontent::Model::Settings::Data.new(options)

          @settings.settings.update(options) unless options.empty?

          if block_given?
            @settings.instance_eval(&block)
            # self.instance_eval(&block)
            # return self
          end

          # init default values
          if @settings.get_option(:project_path).blank?
            @settings.set_option(:project_path, client.settings.fetch(:project_path))
          end

          @settings
        end

        def setup_vproperties

          # relationships _props
          @settings.versions.each do |version, version_props|
            relation_name = vproperties_name(version)
            relation_class = vproperties_class(version)
            relation_table_name = vproperties_table_name(version)

            relation_class.table_name = relation_table_name

            target.has_many relation_name,
                            class_name: relation_class.name,
                            foreign_key: :entity_id,
                            dependent: :destroy,
                            inverse_of: :entity

            target.accepts_nested_attributes_for relation_name

            # preload for relationships
            define_props_preload(relation_name, relation_table_name)
          end

        end

        def setup_methods_attributes
          @settings.versions.each do |version_name, version_props|
            relation_name = vproperties_name(version_name)
            relation_class = vproperties_class(version_name)

            relation_class.attribute_names.each do |attr_name|
              next if ['id', 'entity_id', 'version'].include?(attr_name)

              define_attribute(attr_name, relation_name, version_props)
            end
          end
        end


        def define_props_preload(relation_name, relation_table_name)
          target.scope "preload_#{relation_name}", ->(version) {
            includes(relation_name.to_sym).references(relation_name.to_sym).where("#{relation_table_name}.version" => version)
          }
        end

        def define_attribute(attr_name, relation_name, version_props)
          target.define_method("#{attr_name}") do |*args|
            version = args[0]

            rel = send(relation_name)

            obj_props = nil
            if rel.loaded?
              # should be one version
              if version.nil?
                obj_props = rel.first
              else
                obj_props = rel.select{|x| x.version == version.to_s}
              end
            else
              if args.length == 0
                # use default version from settings
                default_proc = version_props[:default]
                if default_proc && default_proc.respond_to?(:call)
                  version_value = default_proc.call
                  obj_props = rel.where(version: version_value.to_s).first
                else
                  obj_props = rel.where(version: nil).first
                end
              else
                obj_props = rel.where(version: version.to_s).first
              end
            end

            obj_props.send(attr_name)
          end
        end

      end

      module InstanceMethods
        def settings
          target.class.__friendlycontent__.settings
        end
      end

    end
  end
  end
end
