module Friendlycontent
  module Model
  # A proxy interfacing between Friendlycontent::Model instance methods and model instance methods
    class ProxyInstanceMethods
      include ProxyBase

      def klass
        target.class
      end

      def class
        klass.__friendlycontent__
      end

      # def as_json(options={})
      #   target.as_json(options)
      # end

      include Friendlycontent::Model::Concerns::Client::InstanceMethods
      include Friendlycontent::Model::Concerns::Settings::InstanceMethods
      include Friendlycontent::Model::Concerns::DataOperations::InstanceMethods
      include Friendlycontent::Model::Concerns::ProxyVproperties::InstanceMethods

    end
  end
end
