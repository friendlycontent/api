module Friendlycontent
  module Model
    class Schema
      attr_accessor :fields, :versions, :relations



      def fields_by_name
        if @fields_by_name
          return @fields_by_name
        end

        @fields_by_name = {}

        @fields.each do |field|
          @fields_by_name[field.name] = field
        end

        @fields_by_name
      end

    end
  end
end