# Common module for the proxy classes
module Friendlycontent
  module Model
    module ProxyBase
      extend ActiveSupport::Concern

      included do
        attr_reader :target

      end


      def initialize(target)
        @target = target
      end

      # Delegate methods to `@target`
      #
      def method_missing(method_name, *arguments, &block)
        target.respond_to?(method_name) ? target.__send__(method_name, *arguments, &block) : super
      end

      # Respond to methods from `@target`
      #
      def respond_to?(method_name, include_private = false)
        target.respond_to?(method_name) || super
      end

      def inspect
        "[PROXY] #{target.inspect}"
      end

    end
  end
end
