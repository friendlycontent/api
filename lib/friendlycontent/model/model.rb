module Friendlycontent
  module Model
    # Friendlycontent integration for Ruby models
    module Model
      extend ActiveSupport::Concern

      included do
        # include Friendlycontent::Model::Client::InstanceMethods
        # include Friendlycontent::Model::Settings::InstanceMethods

      end

      # Define the `__friendlycontent__` class and instance methods in the including class
      def __friendlycontent__(&block)
        if @__friendlycontent__.nil?
          @__friendlycontent__ = Friendlycontent::Model::ProxyInstanceMethods.new(self)
          @__friendlycontent__.instance_eval(&block) if block_given?
          @__friendlycontent__.init_id
        end

        @__friendlycontent__
      end


      module ClassMethods
        # include Friendlycontent::Model::Client::ClassMethods
        # include Friendlycontent::Model::Settings::ClassMethods

        # setup model
        def friendlycontent(options={}, &block)
          __friendlycontent__.set_settings(options, &block)

          # setup
          __friendlycontent__.setup_vproperties
          __friendlycontent__.setup_methods_attributes

        end


        # proxy class
        def __friendlycontent__(&block)
          if @__friendlycontent__.nil?
            @__friendlycontent__ ||= Friendlycontent::Model::ProxyClassMethods.new(self)
            @__friendlycontent__.instance_eval(&block) if block_given?
          end

          @__friendlycontent__
        end

        def client
          @client ||= Friendlycontent::Model::ModelBase.client
          # @client ||= Friendlycontent::Api::Clientnew.new({})
          @client
        end

        def client=(_client)
          @client = _client
        end


        # def define_field_attributes
        #   __friendlycontent__.settings.fields.fields.each do |field_name, field_opts|
        #     define_method(field_name) do
        #       return __friendlycontent__.field_value(field_name)
        #     end
        #
        #     define_method("#{field_name}=") do |v|
        #       return __friendlycontent__.send("field_value=", field_name, v)
        #     end
        #   end
        # end

      end
      extend ClassMethods

      class NotImplemented < NoMethodError; end
    end
  end
end

