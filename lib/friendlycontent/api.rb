require_relative "api/engine"

require "friendlycontent/model/model_base"
require "friendlycontent/fc_model/content_type_field"
require "friendlycontent/fc_model/content_type_relation"
require "friendlycontent/fc_model/content_type_version"

require "friendlycontent/repository/repository"
require "friendlycontent/repository/search"
require "friendlycontent/repository/content_type_search"
require "friendlycontent/repository/search_request"
require "friendlycontent/repository/record"
require "friendlycontent/repository/cache_base"
require "friendlycontent/repository/cache_disk"
require "friendlycontent/repository/cache_redis"


require "friendlycontent/api/client"
require "friendlycontent/api/response/base"
require "friendlycontent/api/response/error"
require "friendlycontent/api/response/success"
require "friendlycontent/api/response/entity"
require "friendlycontent/api/response/entity_list"
require "friendlycontent/api/response/item_list"

require "friendlycontent/model/model"

require "friendlycontent/model/schema"
require "friendlycontent/model/vproperties"
require "friendlycontent/model/concerns/client"
require "friendlycontent/model/concerns/settings"
require "friendlycontent/model/concerns/data_operations"
require "friendlycontent/model/concerns/proxy_vproperties"
require "friendlycontent/model/settings/data"
require "friendlycontent/model/settings/data_fields"
require "friendlycontent/model/proxy_base"
require "friendlycontent/model/proxy_class_methods"
require "friendlycontent/model/proxy_instance_methods"


# require "zeitwerk"
# loader = Zeitwerk::Loader.for_gem
#
#
# # loader.push_dir File.expand_path("../../../lib", __FILE__)
# loader.push_dir File.expand_path("../api/", __FILE__)
# loader.push_dir File.expand_path("../model", __FILE__)
# loader.push_dir File.expand_path("../repository", __FILE__)
#
# loader.ignore("#{__dir__}/api/engine")
#
# loader.setup


module Friendlycontent
  module Api

  end
end
