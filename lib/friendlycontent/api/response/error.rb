module Friendlycontent
  module Api
  module Response
    class Error < Base

      def initialize(http_response)
        super(http_response)
      end
    end
  end
  end
end
