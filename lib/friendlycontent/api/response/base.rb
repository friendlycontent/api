module Friendlycontent
  module Api
    module Response
      class Base
        attr_accessor :status
        attr_accessor :headers

        # @param [RestClient::Response]
        def initialize(http_response)
          self.status = http_response.code
          self.headers = http_response.headers
        end
    end
    end
  end
end
