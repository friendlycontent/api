module Friendlycontent
  module Api
    module Response
    class Entity < Base
      attr_accessor :payload

      def initialize(http_response)
        super(http_response)

        data = JSON.parse(http_response.body)
        self.payload = data
      end

    end
  end
  end
end
