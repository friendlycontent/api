module Friendlycontent
  module Api
  module Response
    class EntityList < Base
      attr_accessor :payload

      def initialize(http_response)
        super(http_response)

        data = JSON.parse(http_response.body)
        self.payload = data
      end

      def total
        @total ||= (self.headers['x-total-count'] || self.headers[:x_total_count] || 0).to_i
      end

    end
  end
  end
end
