module Friendlycontent
  module Api
    class Engine < ::Rails::Engine
      isolate_namespace Friendlycontent::Api

      # config.watchable_dirs['lib'] = [:rb] if ::Rails.env.development?
      #config.watchable_dirs['app/helpers'] = [:rb] if ::Rails.env.development?

      #config.autoload_paths += Dir["#{Friendlycontent::Api::Engine.root}/lib/"]
      #config.autoload_paths += Dir["#{Friendlycontent::Api::Engine.root}/lib/**/"]
      # config.autoload_paths += Dir["#{Friendlycontent::Api::Engine.root}/lib/friendlycontent/api/"]
      # config.autoload_paths += Dir["#{Friendlycontent::Api::Engine.root}/lib/friendlycontent/api/**/"]

      config.generators do |g|
        g.test_framework :rspec
        g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      end
    end
  end
end
