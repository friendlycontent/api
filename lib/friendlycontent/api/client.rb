module Friendlycontent
  module Api
    class Client
      attr_accessor :settings
      attr_accessor :host

      def initialize(_settings)
        @settings = _settings
        init_settings

      end

      def init_settings
        @host = @settings[:host]
      end

      def build_url(path)
        URI.join @host, path
      end

      def build_query_params (_params={})
        p = _params.compact.stringify_keys
        p
      end

      def build_url_content(project_path, content_type, subpath)
        path = "projects/#{CGI::escape(project_path)}/content/#{content_type}/#{subpath}"
        build_url path
      end

      def do_api_request(url, method, params, payload)
        verify_ssl = self.settings.fetch(:verify_ssl, true)

        # follow redirect
        # https://stackoverflow.com/questions/39279224/how-to-get-redirect-url-withour-following-in-restclient
        #
        # RestClient.post(url, :param => p) do |response, request, result, &block|
        #  if [301, 302, 307].include? response.code
        #     redirected_url = response.headers[:location]
        #  else
        #    response.return!(request, result, &block)
        #  end
        # end

        RestClient::Request.execute(
            url: url.to_s,
            method: method.to_sym,
            headers: {params: params},
            payload: payload,
            verify_ssl: verify_ssl,
            timeout: 300
        )
      end

      def content_get_record(project_path, content_type, id, _params)
        u = build_url_content project_path, content_type, "#{id}"
        p = build_query_params _params
        response = do_api_request(u, :get, p, {})

        if response.code!=200
          return Response::Error.new(response)
        end

        return Response::Entity.new(response)
      end

      def content_update_record(project_path, content_type, id, data)
        u = build_url_content project_path, content_type, "#{id}"
        response = do_api_request(u, :put, {}, data)

        if response.code!=200
          return Response::Error.new(response)
        end

        return Response::Success.new(response)
      end

      def content_delete_record(project_path, content_type, id)
        u = build_url_content project_path, content_type, "#{id}"
        response = do_api_request(u, :delete, {}, {})
        # response = RestClient.delete(u.to_s, {}){|response, request, result| response }

        if response.code!=200
          return Response::Error.new(response)
        end

        return Response::Success.new(response)
      end

      def content_create_record(project_path, content_type, data)
        u = build_url_content project_path, content_type, ""
        response = do_api_request(u, :post, {}, data)
        # response = RestClient.post(u.to_s, data){|response, request, result| response }

        if response.code!=200
          return Response::Error.new(response)
        end

        return Response::Success.new(response)
      end

      def content_search(project_path, content_type, filter, opts)
        u = build_url_content project_path, content_type, "search"

        data = opts.merge({
            filter: filter
        })

        response = do_api_request(u, :post, {}, data)
        # response = RestClient.post(u.to_s, data){|response, request, result| response }

        if response.code!=200
          return Response::Error.new(response)
        end

        Response::EntityList.new(response)
      end

      def content_type_fields(project_path, content_type, opts={})
        u = build_url_content project_path, content_type, "fields"
        data = {}
        response = do_api_request(u, :get, data, {})

        if response.code!=200
          return ResponseError.new(response)
        end

        Response::ItemList.new(response)
      end

      def content_type_versions(project_path, content_type, opts={})
        u = build_url_content project_path, content_type, "versions"
        data = {}
        response = do_api_request(u, :get, data, {})

        if response.code!=200
          return Response::Error.new(response)
        end

        Response::ItemList.new(response)
      end

      def content_type_relations(project_path, content_type, opts={})
        u = build_url_content project_path, content_type, "relations"
        data = {}
        response = do_api_request(u, :get, data, {})

        if response.code!=200
          return Response::Error.new(response)
        end

        Response::ItemList.new(response)
      end
    end
  end
end
