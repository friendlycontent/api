module Friendlycontent
  module Repository
    class CacheRedis < CacheBase
      attr_accessor :options

      # options:
      # :redis - Redis connection object
      # :prefix - Redis key prefix
      # :ttl - TTL in seconds
      def initialize(options={})
        @options = options
      end

      # @param [Redis]
      def redis
        @redis ||= (@options[:redis] || '')
      end

      def prefix
        @prefix ||= (@options[:prefix] || '')
      end

      def ttl
        @ttl ||= (@options[:ttl] || 86400)
      end

      # @param[SearchRequest]
      def get(uid)
        v = redis.get get_key(uid)
        return nil if v.nil?

        # get data
        data = JSON.parse(v) rescue nil
        data
      end

      def put(uid, result)
        key = get_key(uid)
        redis.set key, result.to_json
        redis.expire key, ttl

        true
      end

      def get_key(uid)
        "#{prefix}#{uid}"
      end

    end
  end
end

