module Friendlycontent
  module Repository
    # search request
    class SearchRequest
      DEFAULT_PER_PAGE = 10

      attr_accessor :where
      attr_accessor :versions
      attr_accessor :order

      def initialize

      end

      # @return SearchRequest
      def where(options={})
        @where ||= []

        if !options.empty?
          @where << options
        end

        self
      end

      def get_where
        @where ||= []
      end

      # @param [Integer] pg
      # @return SearchRequest
      def page(pg)
        @page = pg

        self
      end

      def get_page
        @page ||= 1
      end

      # @param [Integer] v
      # @return SearchRequest
      def per_page(v)
        @per_page = v

        self
      end

      def get_per_page
        @per_page ||= DEFAULT_PER_PAGE
      end

      def limit
        get_per_page
      end

      def skip
        (get_page - 1) * get_per_page
      end

      # @param [Hash] order_opts
      def order(order_opts)
        @order = order_opts
      end

      def get_order
        @order ||= {}
      end

      def versions(options={})
        @versions = options

        self
      end

      def get_versions
        @versions ||= {}
      end

    end
  end
end

