module Friendlycontent
  module Repository
    class Repository
      # @return [ Hash ]
      attr_reader :options

      # @example create repository.
      #   Repository.new(client: client, klass: MyRecord, content_type_name: 'my_record')
      # MyRecord should implement instance method :from_hash
      # @param [ Hash ] options The options to use.
      # @option options [ Symbol, String ] :content_type_name the name of the content type
      # @option options [ Symbol, String ] :client client for FriendlyContent API
      # @option options [ Symbol, String ] :klass The class used to instantiate an object when documents are deserialized
      # The default is nil, then the raw document is returned as a Hash (?or Record)
      def initialize(options = {})
        @options = options
      end

      def content_type_name
        @content_type_name ||= @options[:content_type_name]
        #||  __get_class_value(:content_type_name)
      end

      def client
        @client ||= @options[:client]
        #|| __get_class_value(:client)
      end

      # @return [ Class ] The repository's klass for deserializing
      def klass
        @klass ||= @options[:klass] || Record
      end


      # @param [SearchRequest] request
      def find(id, request = nil)
        fc_project_path, fc_content_type_name = get_content_type_parts content_type_name

        api_query_options = {
            query: "",
        }

        if request
          query_versions = build_query_versions request
          api_query_options[:versions] = query_versions
        end

        response = client.content_get_record fc_project_path, fc_content_type_name, id, api_query_options

        # load record
        record = load_record_from_json response.payload

        record
      end

      def save(id, new_fields)
        data = {
            fields: new_fields
        }
        fc_project_path, fc_content_type_name = get_content_type_parts content_type_name

        response = client.content_update_record fc_project_path, fc_content_type_name, id, data

        if response.status != 200
          return false
        end

        true
      end

      def create(new_fields)
        data = {
            fields: new_fields
        }
        fc_project_path, fc_content_type_name = get_content_type_parts content_type_name

        response = client.content_create_record fc_project_path, fc_content_type_name, data

        if response.status != 200
          return false
        end

        data = response.payload
        data['id']
      end

      # @param [Integer] id
      def delete(id)
        fc_project_path, fc_content_type_name = get_content_type_parts content_type_name

        response = client.content_delete_record fc_project_path, fc_content_type_name, id

        if response.status != 200
          return false
        end

        true
      end

      # @param [SearchRequest] request
      def search(request)
        response = search_raw request

        # load records
        records = load_records_from_json response.payload

        records
      end

      # @param [SearchRequest] request
      # @return [Friendlycontent::Api::ResponseEntityList]
      def search_raw(request)
        fc_project_path, fc_content_type_name = get_content_type_parts content_type_name
        query_filter = build_query_filter request
        query_opts = build_query_options request
        response = client.content_search fc_project_path, fc_content_type_name, query_filter, query_opts

        response
      end

      def get_content_type_parts(content_type_name)
        content_type_parts = content_type_name.split /\//
        fc_content_type_name = content_type_parts[-1]
        fc_project_path = content_type_parts.first(content_type_parts.size-1).join('/')

        [fc_project_path, fc_content_type_name]
      end

      def load_records_from_json(data)
        records = []

        list = data
        list.each do |record_data|
          object = load_record_from_json record_data
          records << object
        end

        records
      end

      def load_record_from_json(data)
        object = klass.new
        object.from_hash data

        object
      end

      private

      def __get_class_value(_method_)
        self.class.send(_method_) if self.class.respond_to?(_method_)
      end

      # @param [SearchRequest] request
      def build_query_options(request)
        query_opts = {
            query: build_query_query(request),
            versions: build_query_versions(request),
            page: request.get_page,
            per_page: request.get_per_page,
            order: build_query_order(request),
        }

        query_opts
      end

      def build_query_filter(request)
        where = request.get_where

        filter = {}
        where.each do |w|
          if !w.is_a?(Hash)
            raise 'not implemented'
          end

          w.each do |field_name, v|
            filter[field_name] = v
          end
        end

        filter
      end

      def build_query_query(request)
        query = {}

        query
      end

      def build_query_versions(request)
        request.get_versions
      end

      def build_query_order(request)
        v_order = request.get_order
        a_orders = []
        v_order.each do |f, dir|
          a_orders << "#{f}:#{dir}"
        end

        a_orders.join(",")
      end

    end
  end
end

