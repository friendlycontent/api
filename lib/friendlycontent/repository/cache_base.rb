module Friendlycontent
  module Repository
    class CacheBase

      def get(uid)
        raise 'not implemented'
      end

      def put(uid, result)
        raise 'not implemented'
      end
    end
  end
end

