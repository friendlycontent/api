module Friendlycontent
  module Repository
    class Record
      # @return [Hash]
      attr_accessor :data

      def from_hash(attributes)
        @data = attributes
      end

      def to_hash
        @data
      end

    end
  end
end

