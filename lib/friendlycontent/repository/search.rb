module Friendlycontent
  module Repository
    # search object
    # supports chaining
    # examples
    #
    class Search
      # @return [SearchRequest]
      attr_accessor :request
      attr_accessor :options

      def initialize(options={})
        @options = options
        @request = SearchRequest.new
        @cache ||= @options[:cache]
      end

      def client
        @client ||= @options[:client]
      end

      # @return [CacheBase]
      def cache
        @cache
      end

      # @param [CacheBase]
      def cache=(_cache)
        @cache = _cache
      end

      # ⇒ delegate to request
      # @return [ContentTypeSearch]
      def where(options={})
        request.where(options)
        self
      end

      def versions(options={})
        request.versions(options)
        self
      end

      def page(v)
        request.page(v)
        self
      end

      def per_page(v)
        request.per_page(v)
        self
      end

      # @param [Hash] opts
      def order(opts)
        request.order(opts)
        self
      end


      def load
        raise 'not implemented'
      end

      def find(id, opts={})
        raise 'not implemented'
      end
    end
  end
end

