module Friendlycontent
  module Repository
    class CacheDisk < CacheBase
      DEFAULT_TTL = 86400 * 100

      attr_accessor :options
      attr_accessor :dir_base

      def initialize(options={})
        @options = options
      end

      def dir_base
        @dir_base ||= @options[:dir_base]
      end

      def ttl
        @ttl ||= @options[:ttl] || DEFAULT_TTL
      end

      # @param[SearchRequest]
      def get(uid)
        file_path = get_file uid

        return nil if !File.exists?(file_path)

        mtime = File.mtime(file_path).to_i
        now = Time.now.utc.to_i

        if now - mtime > ttl
          return nil
        end

        # get data from file
        sdata = File.read(file_path)
        data = JSON.parse(sdata) rescue nil

        data
      end

      def put(uid, result)
        file_path = get_file uid

        FileUtils.mkdir_p(File.dirname(file_path))
        File.open(file_path,"w+") do |f|
          f.write result.to_json
        end

        true
      end

      def get_file(uid)
        file_path = File.join(dir_base, uid+".json")
      end
    end
  end
end

