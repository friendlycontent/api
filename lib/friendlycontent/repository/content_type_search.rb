module Friendlycontent
  module Repository
    # search object
    # supports chaining
    # examples
    # example. return generic records
    # search = ContentTypeSearch.new('my_type', client: client)
    # records = search.where(..).page(1).per_page(10).load
    #
    # example. return records of your model
    # search = ContentTypeSearch.new('my_type', client: client, klass: MyRecord)
    # records = search.where(..).page(1).per_page(10).load
    #
    class ContentTypeSearch < Search
      # @return [String] format 'user/project/type'. example: 'myuser/myproject/product'
      #
      attr_accessor :content_type_name

      # @param [String] _content_type_name
      def initialize(_content_type_name, options={})
        super options

        @content_type_name = _content_type_name
      end

      # @return [ Class ] The repository's klass for deserializing
      def klass
        @klass ||= @options[:klass]
      end

      def client
        @client ||= @options[:client]
      end

      def repo
        @repo ||= Friendlycontent::Repository::Repository.new(klass: klass, content_type_name: content_type_name, client: client)
      end

      # @return [Array]
      def load
        result = load_with_cache(request) do
          repo.search(request)
        end

        result
      end

      def load_with_cache(request, &block)
        # no cache available
        if cache.nil?
          result = yield

          return result
        end

        # use cache
        cache_uid = build_request_uid({content_type_name: content_type_name, action: 'search', request: request.to_json})

        result_cache = cache.get(cache_uid)

        if !result_cache.nil?
          return repo.load_records_from_json result_cache
        end

        result = yield

        cache.put(cache_uid, result)

        result
      end

      def build_request_uid(options)
        require 'digest/sha1'
        s = options.to_json
        Digest::SHA1.hexdigest s
      end

      def find(id, opts={})
        use_cache = opts[:cache].nil? ? true : false

        if use_cache
          result = find_with_cache(id, request) do
            repo.find(id, request)
          end
        else
          result = repo.find(id, request)
        end

        result
      end

      def find_with_cache(id, request, &block)
        # no cache available
        if cache.nil?
          result = yield

          return result
        end

        # use cache
        cache_uid = build_request_uid({content_type_name: content_type_name, action: 'find', id: id, request: request.to_json})

        result_cache = cache.get(cache_uid)

        if !result_cache.nil?
          return repo.load_record_from_json result_cache
        end

        result = yield

        cache.put(cache_uid, result)

        result
      end

      def first
        first_with_cache(request) do
          request.page(1)
          request.per_page(1)
          records = repo.search(request)

          records.count > 0 ? records[0] : nil
        end
      end

      def first_with_cache(request, &block)
        # no cache available
        if cache.nil?
          result = yield

          return result
        end

        # use cache
        cache_uid = build_request_uid({content_type_name: content_type_name, action: 'first', request: request.to_json})
        result_cache = cache.get(cache_uid)

        if !result_cache.nil?
          result = repo.load_record_from_json result_cache
          return result
        end

        # calc
        result = yield

        # save to cache
        cache.put(cache_uid, result)

        result
      end
    end
  end
end

