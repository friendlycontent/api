module Friendlycontent
  module FcModel
    class ContentTypeVersion
      attr_accessor :id, :name, :title, :pos, :values

      def self.create_from_data(data)
        object = ContentTypeVersion.new

        %w(name title values).each do |f|
          object.send(:"#{f}=", data[f])
        end

        object
      end

      def id
        self.name
      end
    end
  end
end

