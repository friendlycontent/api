module Friendlycontent
  module FcModel
    class ContentTypeRelation
      attr_accessor :id, :name, :title, :ref_content_type, :type, :ref_field,
                    :through_content_type, :through_ref_field

      def self.create_from_data(data)
        object = ContentTypeRelation.new

        %w(name title ref_content_type type ref_field through_content_type through_ref_field).each do |f|
          object.send(:"#{f}=", data[f])
        end

        object
      end

      def id
        self.name
      end
    end
  end
end

