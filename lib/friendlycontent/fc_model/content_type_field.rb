module Friendlycontent
  module FcModel
    class ContentTypeField
      attr_accessor :id, :name, :title, :pos, :type, :versions

      def self.create_from_data(data)
        object = ContentTypeField.new

        %w(name title pos type versions).each do |f|
          object.send(:"#{f}=", data[f])
        end

        object
      end

      def id
        self.name
      end

      def pk?
        self.name == 'id'
      end

      def versioned?
        !@versions.empty?
      end

      def to_db_type(db=nil)
        db ||= ActiveRecord::Base.connection.adapter_name

        name = case db
               when 'Mysql2', 'mysql', 'MySql'
                 'mysql'
               else
                 ''
               end

        send(:"to_db_type_#{name}")
      end

      def to_db_type_mysql
        case type
        when 'string'
          'string'
        when 'text'
          'text'
        when 'integer', 'int'
          'integer'
        when 'bool', 'boolean'
          'boolean'
        else
          nil
        end
      end
    end
  end
end

