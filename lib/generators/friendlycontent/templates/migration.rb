class Create<%= @class_name %> < ActiveRecord::Migration[<%= Rails.gem_version.segments[0..1].join(".") %>]
  def change
    create_table :<%= @table_name %>, id: :bigint, force: true do |t|
      <% @fields.each do |field| %>
        <% next if field.pk? || field.versioned? %>
        t.<%= field.to_db_type %> :<%=field.name %>, null: true
      <% end %>
    end

    <% @versions.each do |version| %>
      <% versioned_table_name = "#{@table_name}_#{version.name}_props"%>

    create_table :<%= versioned_table_name %>, id: :bigint, force: true do |t|
      t.bigint :entity_id, null: false
      t.string :version, null: true


      <% @fields_by_version[version.name].each do |field| %>
        t.<%= field.to_db_type %> :<%=field.name %>, null: true
      <% end %>
    end

    add_index :<%= versioned_table_name %>, [:entity_id, :version], name: "<%= versioned_table_name %>_main_idx"

    <% end %>


  end
end