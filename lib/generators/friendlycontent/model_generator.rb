# frozen_string_literal: true

# require 'rails/generators'
require 'rails/generators/named_base'

module Friendlycontent
  module Generators
    # rails g friendlycontent:model NAME
    # examples,
    # rails g friendlycontent:model product
    class ModelGenerator < ::Rails::Generators::NamedBase
      # include ActiveRecord::Generators::Base
      include Rails::Generators::Migration
      # include Rails::Generators::ResourceHelpers

      source_root File.expand_path('../templates', __FILE__)

      def generate_model
        # puts "name: #{name}, class: #{class_name}"

        #   template 'foo', 'bar', :assigns => { :action => 'view' }
        template "model.rb", "app/models/#{name}.rb"

        # db migrations
        generate_model_migration

      end

      private

      def generate_model_migration
        @class_name = "#{class_name}"
        @table_name = name.singularize.underscore
        @vproperties_table_name = name.singularize.underscore+"_vproperties"

        # get schema
        client = fc_api_client
        project_path = Rails.application.config.friendlycontent_config[:project_path]

        response = client.content_type_fields(project_path, @class_name)
        @fields = response.payload.map{|field_data| Friendlycontent::FcModel::ContentTypeField.create_from_data(field_data) }

        response = client.content_type_versions(project_path, @class_name)
        @versions = response.payload.map{|version_data| Friendlycontent::FcModel::ContentTypeVersion.create_from_data(version_data) }

        # puts "fields: #{@fields.inspect}"
        # puts "versions: #{@versions.inspect}"

        @fields_by_version = {}
        @versions.each do |version|
          @fields_by_version[version.name] = []
        end

        @fields.each do |field|
          next unless field.versioned?

          field.versions.each do |version|
            @fields_by_version[version] << field
          end

        end

        migration_template "migration.rb", "db/migrate/create_#{name}.rb"
      end

      def fc_api_client
        Friendlycontent::Api::Client.new(Rails.application.config.friendlycontent_config)
      end

      def self.next_migration_number(dirname)
        next_migration_number = current_migration_number(dirname) + 1
        ActiveRecord::Migration.next_migration_number(next_migration_number)
      end

    end
  end
end
